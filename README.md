# newtop-dist-proc

As of 2004 (http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.110.6701&rep=rep1&type=pdf), within the world of total order broadcast and multicast algorithms, there were five general classes of algorithms: communication history, privilege-based, moving sequencer, fixed sequencer, and destinations agreement. According to a table within the 2004 survey, Newtop is a somewhat fault tolerant member of the communcation history class. The related work section of paper on Ring Paxos (https://arxiv.org/abs/1401.6015) provides an interesting discussion of tradeoffs between the five general classes of algorithms. 

This repository provides a basic implemenation of total order broadcast using Newtop (New Castle Total Order Protocol) (https://pdfs.semanticscholar.org/419b/876a26e83c94b1b02443011a690bf63e79a8.pdf). The implementation roughly follows the paper, implementing the symmetric, fault tolerant version. The implementation does not form additional groups when encountering partitions, nor does it explicitly support forming multiple groups. A given process is expected to either remain alive or fail by stopping.

Build and run as follows

```
$ stack build && stack exec newtop-dist-proc-exe -- --send-for 10 --wait-for 10
Sat Jun 10 04:50:17 UTC 2017 pid://localhost:3001:0:11: peers:2
Sat Jun 10 04:50:17 UTC 2017 pid://localhost:3002:0:11: peers:[pid://localhost:3002:0:11,pid://localhost:3001:0:11]
Sat Jun 10 04:50:32 UTC 2017 pid://localhost:3002:0:11: pend cnt:2
Sat Jun 10 04:50:32 UTC 2017 pid://localhost:3002:0:11: pend:[(0.924266813040276,10638,pid://localhost:3001:0:11),(0.6042800078117163,10637,pid://localhost:3001:0:11)]
Sat Jun 10 04:50:32 UTC 2017 pid://localhost:3002:0:11: ProcState {stClock = 10638, stReceiveVector = fromList [(pid://localhost:3001:0:11,(10638,1497070227.423289s)),(pid://localhost:3002:0:11,(10636,1497070227.381027s))], stPendingDeliver = [], stMemberView = [pid://localhost:3002:0:11,pid://localhost:3001:0:11]}
Sat Jun 10 04:50:32 UTC 2017 pid://localhost:3001:0:11: pend cnt:2
Sat Jun 10 04:50:32 UTC 2017 pid://localhost:3001:0:11: pend:[(0.924266813040276,10638,pid://localhost:3001:0:11),(0.6042800078117163,10637,pid://localhost:3001:0:11)]
Sat Jun 10 04:50:32 UTC 2017 pid://localhost:3001:0:11: ProcState {stClock = 10638, stReceiveVector = fromList [(pid://localhost:3001:0:11,(10638,1497070227.382318s)),(pid://localhost:3002:0:11,(10636,1497070227.381072s))], stPendingDeliver = [], stMemberView = [pid://localhost:3002:0:11,pid://localhost:3001:0:11]}
(14832,5.535339421391653e7)
(14832,5.535339421391645e7)
```

One can modify the configuratin in `LibMain.hs` by providing the number of slaves and a schedule of when to kill which processes. The schedule consists of pairs, where the first value is how many seconds to wait from the last event, and the second value is a list of 0-based indexes indicating slaves to stop/force failure at the given instant.

The following schedule indicates that slave 0 will be killed at second 1, and slave 1 will be killed at second 5 (4+1).

```
killSchedule :: [(Int, [Int])]
killSchedule = [(1,[0]), (4,[1])]

numSlaves :: Int
numSlaves = 5
```
